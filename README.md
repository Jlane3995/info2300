README: ASSIGNMENT4REDUX

RUN PROGRAM:

1) Install Microsoft Visual Studio 2019 available here: https://visualstudio.microsoft.com/vs/

2) Keep the default settings during installation process. 

3) After installation, open Microsoft Visual Studio 2019 and select "Open a project or solution". 

4) Navigate to the Assignment4Redux folder and double click. Double click the .sln file. 

5) Once Visual Studio has opened the solution, press the play icon at top center of the screen or press F5 to run the solution without debugging.

INSTALLATION:

1) In Visual Studio, proceed to the Build menu, select "Publish", click "Start", select "Folder", select "Start".


LICENCING:

The reason this software has an opensource licence is because it's on a public repository.


**Here's a change I made for the last part of the assignment**