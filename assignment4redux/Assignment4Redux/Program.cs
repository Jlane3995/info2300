﻿using System;

namespace Assignment4Redux
{
    class Program
    {
        const short MAX_TABLES = 6;
        const short MAX_CHAIRS = 4;
        static string[,] seatingPlan = new string[MAX_TABLES, MAX_CHAIRS];
        static bool isProgramRunning = false;

        static void Main(string[] args)
        {
            do
            {
                Console.WriteLine("Welcome to another wedding that will inexorably lead to catastrophe. Please choose one of the following options: ");
                Console.WriteLine("1. Add a guest.");
                Console.WriteLine("2. Remove a guest.");
                Console.WriteLine("3. Display the seating plan.");
                Console.WriteLine("4. Get me out of here! ");
                Console.Write("Your selection: ");

                short userChoice;
                while (!short.TryParse(Console.ReadLine(), out userChoice))
                {
                    Console.Write("Choose an option from 1-4: ");
                    //short.TryParse(Console.ReadLine(), out userChoice);
                }

                while (userChoice < 1 || userChoice > 4)
                {
                    Console.Write("Enter a number from 1-4: ");
                    short.TryParse(Console.ReadLine(), out userChoice);
                }

                switch (userChoice)
                {
                    case 1:
                        AddGuest();
                        break;
                    default:
                        break;

                    case 2:
                        RemoveGuest();
                        break;

                    case 3:
                        DisplaySeatingPlan();
                        break;

                    case 4:
                        Console.Beep();
                        isProgramRunning = true;
                        break;
                }

            } while (!isProgramRunning);
        }


        static void AddGuest()
        {
            Console.Write("Enter your guest's name: ");
            string guestName = Console.ReadLine();

            while (string.IsNullOrEmpty(guestName))
            {
                Console.Write("Please enter your guest's name: ");
                guestName = Console.ReadLine();
            }

            Console.Write("Enter a number from 1-6 to select your table: ");
            int tableNumber;

            while (!int.TryParse(Console.ReadLine(), out tableNumber))
            {
                Console.Write("Enter a number from 1-6 to select a table: ");
            }

            while (tableNumber < 1 || tableNumber > 6)
            {
                Console.Write("Enter a number from 1-6 to select a table: ");
                int.TryParse(Console.ReadLine(), out tableNumber);
            }

            tableNumber = tableNumber - 1;

            Console.Write("Enter a number from 1-4 to select your seat: ");
            int seatNumber;

            while (!int.TryParse(Console.ReadLine(), out seatNumber))
            {
                Console.Write("Enter a number from 1-4 to select a seat: ");
            }

            while (seatNumber < 1 || seatNumber > 4)
            {
                Console.Write("Enter a number from 1-4 to select a seat: ");
                int.TryParse(Console.ReadLine(), out seatNumber);
            }

            seatNumber = seatNumber - 1;

            seatingPlan[tableNumber, seatNumber] = guestName;

        }

        static void RemoveGuest()
        {
            Console.Write("Press 1 to remove a guest by table and seat, press 2 to search for a guest by name: ");

            short userSelection;
            while (!short.TryParse(Console.ReadLine(), out userSelection))
            {
                Console.Write("Choose an option from 1-2: ");
                //short.TryParse(Console.ReadLine(), out userChoice);
            }

            while (userSelection < 1 || userSelection > 2)
            {
                Console.Write("Enter a number from 1-2: ");
                short.TryParse(Console.ReadLine(), out userSelection);
            }

            switch (userSelection)
            {
                case 1:
                    int tableNumber;

                    while (!int.TryParse(Console.ReadLine(), out tableNumber))
                    {
                        Console.Write("Enter a number from 1-6 to select a table: ");
                    }

                    while (tableNumber < 1 || tableNumber > 6)
                    {
                        Console.Write("Enter a number from 1-6 to select a table: ");
                        int.TryParse(Console.ReadLine(), out tableNumber);
                    }

                    tableNumber = tableNumber - 1;

                    Console.Write("Enter a number from 1-4 to select a seat: ");
                    int seatNumber;

                    while (!int.TryParse(Console.ReadLine(), out seatNumber))
                    {
                        Console.Write("Enter a number from 1-4 to select a seat: ");
                    }

                    while (seatNumber < 1 || seatNumber > 4)
                    {
                        Console.Write("Enter a number from 1-4 to select a seat: ");
                        int.TryParse(Console.ReadLine(), out seatNumber);
                    }

                    seatNumber = seatNumber - 1;

                    if (string.IsNullOrEmpty(seatingPlan[tableNumber, seatNumber]))
                    {
                        Console.WriteLine("There is no guest in this spot. ");
                    }

                    if (!string.IsNullOrEmpty(seatingPlan[tableNumber, seatNumber]))
                    {
                        Console.WriteLine(seatingPlan[tableNumber, seatNumber] + " has been removed.");
                        seatingPlan[tableNumber, seatNumber] = "";
                    }

                    break;

                case 2:

                    bool isRemovingGuestByName = false;
                    do
                    {
                        Console.Write("Enter the name of the guest you want to remove: ");

                        string guestName = Console.ReadLine();

                        while (string.IsNullOrEmpty(guestName))
                        {
                            Console.Write("Enter the name of the guest you want to remove: ");
                            guestName = Console.ReadLine();
                        }

                        for (int i = 0; i < seatingPlan.GetLength(0); i++)
                        {
                            for (int j = 0; j < seatingPlan.GetLength(1); j++)
                            {
                                if (string.Equals(seatingPlan[i, j], guestName))
                                {
                                    seatingPlan[i, j] = null;

                                }
                            }
                            isRemovingGuestByName = true;
                        }
                        Console.WriteLine(guestName + " has been removed. ");
                    } while (!isRemovingGuestByName);

                    break;
            }

        }
        static void DisplaySeatingPlan()
        {
            for (int i = 0; i < seatingPlan.GetLength(0); i++)
            {
                for (int j = 0; j < seatingPlan.GetLength(1); j++)
                {
                    Console.WriteLine(seatingPlan[i, j]);

                    if (string.IsNullOrEmpty(seatingPlan[i, j]))
                    {
                        Console.WriteLine("Empty seat. ");

                    }
                }
                Console.WriteLine();
            }
        }
    }
}
